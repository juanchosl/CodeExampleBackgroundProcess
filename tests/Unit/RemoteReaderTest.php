<?php

namespace CodeExampleBackgroundProcess\Tests\Unit;

use PHPUnit\Framework\TestCase;
use CodeExampleBackgroundProcess\Domain\Entities\User;
use CodeExampleBackgroundProcess\Infrastructure\Parser\RemoteJsonUrlDataReader;

class RemoteReaderTest extends TestCase
{

    public function testValidReadingConversion()
    {
        $users = (new RemoteJsonUrlDataReader('https://jsonplaceholder.typicode.com/users'))->fill();
        $this->assertIsArray($users);
        $this->assertNotEmpty($users);
        foreach ($users as $user) {
            $this->assertIsObject($user);
            $this->assertInstanceOf(User::class, $user);
            $this->assertObjectHasAttribute('id', $user);
            $this->assertObjectHasAttribute('fullname', $user);
            $this->assertObjectHasAttribute('phone', $user);
            $this->assertObjectHasAttribute('company', $user);
            $this->assertObjectHasAttribute('email', $user);
        }
    }

}
