<?php

namespace CodeExampleBackgroundProcess\Tests\Functional;

use PHPUnit\Framework\TestCase;
use CodeExampleBackgroundProcess\Application\ReadAndWriteUseCase;

class LocalWriterTest extends TestCase
{

    public function testValidReadingAndWriting()
    {
        $reader1 = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'etc' . DIRECTORY_SEPARATOR . 'in' . DIRECTORY_SEPARATOR . 'data.xml';
        $reader2 = 'https://jsonplaceholder.typicode.com/users';
        $writer = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'export_' . uniqid() . ".csv";

        $this->assertTrue((new ReadAndWriteUseCase())($writer, [], $reader1, $reader2));
    }

}
