<?php

namespace CodeExampleBackgroundProcess\Tests\Integration;

use PHPUnit\Framework\TestCase;
use CodeExampleBackgroundProcess\Infrastructure\Repository\LocalCsvFileDataWriter;
use CodeExampleBackgroundProcess\Infrastructure\Parser\LocalXmlFileDataReader;
use CodeExampleBackgroundProcess\Infrastructure\Parser\RemoteJsonUrlDataReader;
use CodeExampleBackgroundProcess\Domain\ReaderService;
use CodeExampleBackgroundProcess\Domain\Entities\User;

class LocalWriterTest extends TestCase
{

    public function testValidReadingAndWriting()
    {
        $service = new ReaderService();
        $service->readData(new LocalXmlFileDataReader(dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'etc' . DIRECTORY_SEPARATOR . 'in' . DIRECTORY_SEPARATOR . 'data.xml'));
        $count_with_client = count($service->getData());
        $this->assertGreaterThan(0, $count_with_client);
        $service->readData((new RemoteJsonUrlDataReader('https://jsonplaceholder.typicode.com/users')));
        $count_with_remote = count($service->getData());
        $this->assertGreaterThan($count_with_client, $count_with_remote);
        $this->assertContainsOnlyInstancesOf(User::class, $service->getData());
        $this->assertTrue($service->writeData(new LocalCsvFileDataWriter(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'export_' . time() . ".csv")));
    }

}
