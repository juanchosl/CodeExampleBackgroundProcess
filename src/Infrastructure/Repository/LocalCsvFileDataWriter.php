<?php

namespace CodeExampleBackgroundProcess\Infrastructure\Repository;

use CodeExampleBackgroundProcess\Domain\Contracts\DataWriterInterface;

class LocalCsvFileDataWriter implements DataWriterInterface
{

    private $source;

    /**
     *
     * @param string $source Local file to create
     */
    public function __construct(string $source)
    {
        $this->source = $source;
    }

    /**
     * Create a new CSV and fill it with the array values
     * @param array $array Values
     * @param array $columns Fields to include into file
     */
    public function fill(array $array, array $columns = []): bool
    {
        $file = fopen($this->source, 'x');
        if ($file === false) {
            throw new Exception("The destiny file {$this->source} can not be created");
        }
        foreach ($array as $row) {
            $row = (array) $row;
            if (empty($setted_columns)) {
                if (empty($columns)) {
                    $columns = array_keys($row);
                }
                fputcsv($file, $columns);
                $setted_columns = true;
            }
            $line = [];
            foreach ($columns as $column) {
                $line[$column] = $row[$column];
            }
            fputcsv($file, $line);
        }

        return fclose($file);
    }

}
