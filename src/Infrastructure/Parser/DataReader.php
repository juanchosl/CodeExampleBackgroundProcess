<?php

namespace CodeExampleBackgroundProcess\Infrastructure\Parser;

abstract class DataReader
{

    protected $source;
    protected $data;

    /**
     *
     * @param string $source
     */
    public function __construct(string $source)
    {
        $this->source = $source;
        $this->read();
    }

    abstract protected function read(): void;
}
