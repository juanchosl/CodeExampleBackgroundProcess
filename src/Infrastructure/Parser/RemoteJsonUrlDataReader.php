<?php

namespace CodeExampleBackgroundProcess\Infrastructure\Parser;

use CodeExampleBackgroundProcess\Domain\Entities\User;
use CodeExampleBackgroundProcess\Domain\Contracts\DataReaderInterface;

class RemoteJsonUrlDataReader extends DataReader implements DataReaderInterface
{

    /**
     * Read the source content and parse as json
     * @return void
     * @throws \Exception If source is not available or if his response is not a valid json
     */
    protected function read(): void
    {
        if (($content = file_get_contents($this->source)) === false) {
            throw new \Exception("Data from {$this->source} is not available");
        }
        $this->data = json_decode($content);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception("Data from {$this->source} is not a valid JSON content: " . json_last_error_msg());
        }
    }

    /**
     * Fill an array (or create a new one) in order to enlarge his content
     * @param array $iterator Values
     * @return array Filled array
     */
    public function fill(array $iterator = []): array
    {
        foreach ($this->data as $user) {
            $new_user = new User();
            $new_user->id = $user->id;
            $new_user->fullname = $user->name;
            $new_user->email = $user->email;
            $new_user->phone = $user->phone;
            $new_user->company = $user->company->name;
            array_push($iterator, $new_user);
            unset($new_user);
        }
        return $iterator;
    }

}
