<?php

namespace CodeExampleBackgroundProcess\Infrastructure\Parser;

use CodeExampleBackgroundProcess\Domain\Entities\User;
use CodeExampleBackgroundProcess\Domain\Contracts\DataReaderInterface;

class LocalXmlFileDataReader extends DataReader implements DataReaderInterface
{

    /**
     * Read the source content
     * @return void
     * @throws \Exception if source is not available
     */
    protected function read(): void
    {
        if (!file_exists($this->source)) {
            throw new \Exception("The file {$this->source} not exists");
        }
        $this->data = new \SimpleXMLElement(file_get_contents($this->source));
    }

    /**
     * Fill an array (or create a new one) in order to enlarge his content
     * @param array $iterator Values
     * @return array Filled array
     */
    public function fill(array $iterator = []): array
    {
        foreach ($this->data->reading as $user) {
            $new_user = new User();
            $new_user->id = (int) $user[0]->attributes()->clientID;
            $new_user->fullname = (string) $user[0]->attributes()->name;
            $new_user->email = (string) $user;
            $new_user->phone = (string) $user[0]->attributes()->phone;
            $new_user->company = (string) $user[0]->attributes()->company;
            array_push($iterator, $new_user);
            unset($new_user);
        }
        return $iterator;
    }

}
