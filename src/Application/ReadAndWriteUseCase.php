<?php

namespace CodeExampleBackgroundProcess\Application;

use CodeExampleBackgroundProcess\Domain\ReaderService;
use CodeExampleBackgroundProcess\Infrastructure\Parser\LocalXmlFileDataReader;
use CodeExampleBackgroundProcess\Infrastructure\Parser\RemoteJsonUrlDataReader;
use CodeExampleBackgroundProcess\Infrastructure\Repository\LocalCsvFileDataWriter;
use CodeExampleBackgroundProcess\Domain\Contracts\DataReaderInterface;

class ReadAndWriteUseCase
{

    public function __invoke($destiny, array $columns = [], string ...$origins)
    {
        $service = new ReaderService();
        foreach ($origins as $origin) {
            if (is_file($origin)) {
                switch (strtoupper(pathinfo($origin, PATHINFO_EXTENSION))) {
                    case 'XML':
                        $parser = new LocalXmlFileDataReader($origin);
                        break;
                }
            } elseif (filter_var($origin, FILTER_VALIDATE_URL)) {
                $parser = new RemoteJsonUrlDataReader($origin);
            } else {
                continue;
            }
            if ($parser instanceof DataReaderInterface) {
                $service->readData($parser);
            }
        }
        return $service->writeData(new LocalCsvFileDataWriter($destiny), $columns);
    }

}
