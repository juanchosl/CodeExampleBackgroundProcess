<?php

namespace CodeExampleBackgroundProcess\Domain\Contracts;

interface DataReaderInterface
{

    public function fill(array $iterator = []): array;
}
