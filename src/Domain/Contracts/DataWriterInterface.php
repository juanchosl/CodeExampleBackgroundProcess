<?php

namespace CodeExampleBackgroundProcess\Domain\Contracts;

interface DataWriterInterface
{

    public function __construct(string $filepath);

    public function fill(array $array, array $columns = []): bool;
}
