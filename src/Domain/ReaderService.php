<?php

namespace CodeExampleBackgroundProcess\Domain;

use CodeExampleBackgroundProcess\Domain\Contracts\DataReaderInterface;
use CodeExampleBackgroundProcess\Domain\Contracts\DataWriterInterface;

class ReaderService
{

    private $data = [];

    public function readData(DataReaderInterface ...$readers)
    {
        foreach ($readers as $reader) {
            $this->data = $reader->fill($this->data);
        }
    }

    public function writeData(DataWriterInterface $writer, array $columns = [])
    {
        return $writer->fill($this->data, $columns);
    }

    public function getData()
    {
        return $this->data;
    }

}
