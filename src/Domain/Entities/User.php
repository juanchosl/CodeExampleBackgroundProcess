<?php

namespace CodeExampleBackgroundProcess\Domain\Entities;

class User
{

    public $id;
    public $email;
    public $fullname;
    public $phone;
    public $company;

}
