## Developer
Juan Sánchez Lecegui <JuanchoSL@hotmail.com>

## Descripción
La presente task permite generar un archivo CSV compuesto de los datos obtenidos de diferentes fuentes.

## Sistema
El proceso está creado y probado usando PHP 8.1

## Dependencias
El proceso no requiere de dependencias externas, se usa __composer__ para el autoloader de clases y sólo incluye __phpunit__ para realizar tests.

Se han preferido usar las librerías nativas disponibles para evitar la dependencia de terceros y evitar que cambios en los repositorios externos afecten al funcionamiento del proceso.

## Instalación
Instalar composer para activar autoload

```
composer install
```

## Ejecución
Para ejecutar el proceso, ir a la carpeta bin del proyecto y ejecutar _process_

```
$bin/>./process
```

## Resultado
La carpeta *etc* contiene tanto el fichero de cliente (fuente de datos en directorio *in*) como las diferentes salidas csv generadas (directorio *out*), con el timestamp de la creación en el nombre del fichero, del tipo *export_NNNNNNNNNN.csv*

## Tests

Para ejecutar los tests, podemos
- lanzar __phpunit__ desde nuestra propia instalación, carpeta *tests*
- lanzar __phpunit__ desde el vendor
- usar los scripts agregados mediante __composer__

### Tests unitarios

```
vendor/bin/phpunit --configuration phpunit.xml tests/Unit
```
```
composer tests_unit
```

### Tests de integración

```
vendor/bin/phpunit --configuration phpunit.xml tests/Integration
```
```
composer tests_integration
```

### Tests funcionales

```
vendor/bin/phpunit --configuration phpunit.xml tests/Functionals
```
```
composer tests_functional
```

### Todos los tests

```
vendor/bin/phpunit --configuration phpunit.xml tests
```
```
composer tests
```
